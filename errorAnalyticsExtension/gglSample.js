/* Google Code for Add to Cart Conversion Page
	In your html page, add the snippet and call goog_report_conversion
  when someone clicks on the chosen link or button. 
  */
goog_snippet_vars = function() {
  var w = window;
  w.google_conversion_id = 12345678;
  w.google_conversion_label = "abcDeFGHIJklmN0PQ";
  w.google_conversion_value = 13.00;
  w.google_conversion_currency = "USD";
  w.google_remarketing_only = false;
}
// DO NOT CHANGE THE CODE BELOW.
goog_report_conversion = function(url) {
  goog_snippet_vars();
  window.google_conversion_format = "3";
  var opt = new Object();
  opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}

/*
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion_async.js">
</script>
*/
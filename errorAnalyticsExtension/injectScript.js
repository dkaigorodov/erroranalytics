
(function () {

  const x = (function () {
    var domain;
    
    var kuollRecordId; // record start time
    var frameViewId;
    var gCid; // google analytics clientId
    var startDateTime = Date.now();
    var iframe = (function () {
      try {
        return window.self !== window.top;
      } catch (e) {
        return true;
      }
    })();
    
    var prevEcomAction;
    var prevEcomActionMks;

    function getDomain() {
      var host = document.location.host;
      var lastDotIndex = host.lastIndexOf('.');
      var cutIndex = host.substr(0, lastDotIndex).lastIndexOf('.')
      domain = host.substr(cutIndex + 1);
    }

    function uuidv4() {
      return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, function(c) {
        return (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16);
      })
    }

    var timeMks = (performance && performance.now) ? performance.now.bind(performance): function() {
      return Date.now() - startDateTime;
    };

    return {
      start: function() {
        domain = getDomain();
        if (localStorage) {
          kuollRecordId = localStorage.kuollRecordId;
          if (!kuollRecordId) {
            localStorage.kuollRecordId = kuollRecordId = uuidv4();
          }
        }
        frameViewId = uuidv4();
        startDateTime = Date.now();
        prevEcomAction = "initScriptLoad";
        prevEcomActionMks = timeMks();
        ga(function(tracker) {
          gCid = tracker.get('clientId');;
        });

        return this;
      }, 

      send: function(e, cb) {      
        var url = "https://convmon.kuoll.com/";
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);

        xhr.onabort = function () {
        };
        xhr.onerror = function () {
          console.error("XHR request failed", null, url);
          xhr = null;
        };
        xhr.addEventListener("loadend", function () {
          xhr = null;
          if (cb) {
            try {
              cb(this.response);
            } catch (e) {
              console.log("Error in API request callback", e);
            }
          }
        });
        xhr.setRequestHeader("Content-Type", "text/plain; charset=UTF-8");
        xhr.send(JSON.stringify(e));
      },

      enhance: function(event) {
        event.domain = domain;
        event.recordId = kuollRecordId;
        event.frameViewId = frameViewId;
        event.gtmSessionId = gCid;
        event.iframe = iframe;
        event.url = document.location.href;
        event.vw = document.documentElement.clientWidth;
        event.vh = document.documentElement.clientHeight;
        event.dw = window.screen.width;
        event.dh = window.screen.height;
        event.pw = document.body.clientWidth; // p for page
        event.ph = document.body.clientHeight;
        event.pw = document.body.clientWidth; 
        event.ph = document.body.clientHeight;
        event.vx = window.scrollX;
        event.vy = window.scrollY;
        

        var nowMks = timeMks();
        event.timeMks = nowMks;
        event.prevEcomTime = nowMks - prevEcomActionMks; 
        event.dateTime = Date.now();
        
        return event;
      },

      event: function(event) {
        this.enhance(event);
        this.send(event);
        return event;
      },

      ecommerce: function(ecomEvent) {
        if (ecomEvent.type != "ecommerce") {
          throw new Error("Not ecommerce event: " + JSON.stringify(ecomEvent));
        }

        this.event(ecomEvent);

        prevEcomAction = ecomEvent.eventAction;
        prevEcomActionMks = ecomEvent.timeMks;
      }
    }
  })().start();
  // end of 'x'


  function proxy(objName, fname, cb) {
    var oldImpl = window[objName][fname];

    if(oldImpl.kuollProxied) {
      throw new Error('Function already proxied: ' + fname);
    }

    var proxy = function() {
      var args = arguments;
      var retVal;

      try {
        retVal = oldImpl.apply(window, arguments);
      } catch(e) {
        x.event({
          type: 'error',
          error: e,
          hint: 'proxiedOrig',
          fname: "window." + objName + "." + fname,
          args: args
        });

        throw e;
      }

      try {
        cb(args, retVal);
      } catch(e) {
        console.log(e);
        x.event({
          type: 'error',
          error: e,
          hint: 'proxiedCb',
          fname: "window." + objName + "." + fname,
          args: args
        });
      }

      return retVal;
    }

    proxy.kuollProxied = oldImpl;

    window[objName][fname] = proxy;
  }

  function proxyWhenReady(objName, fname, initCb, cb) {
    if (
      window[objName] &&
      typeof(window[objName][fname]) == "function"
    ) {      
      initCb();
      proxy(objName, fname, cb);
    } else {
      setTimeout(function () {
        proxyWhenReady(objName, fname, initCb, cb);
      }, 50);
    }
  }

  function proxyGtmDataLayer() {
    proxyWhenReady("dataLayer", "push", function () {
      x.event({
        type: "init",
        hint: "dataLayer",
        data: dataLayer
      });
    }, function(args, retVal) {
      var data = args[0];
      if(data && data.ecommerce && !data.eventNonInteraction) {
        var ecommerce = data.ecommerce;
        var step = ecommerce.checkout && ecommerce.checkout.actionField && ecommerce.checkout.actionField.step;
        var hint = 
          data.eventAction ||
          (!!ecommerce.purchase ? "purchase": false) || 
          (!!ecommerce.refund ? "refund": false) || 
          (!!ecommerce.add ? "add": false) || 
          (!!ecommerce.remove ? "remove": false) || 
          (!!ecommerce.detail ? "detail": false) || 
          (!!ecommerce.checkout ? "checkout": false) || 
          (!!ecommerce.checkout_option ? "checkout_option": false) || 
          (!!ecommerce.promo_click ? "promo_click": false) || 
          (!!ecommerce.click ? "click": false);
        x.ecommerce({
          type: "ecommerce",
          hint: hint,
          interaction: !data.eventNonInteraction,
          eventAction: ecommerce.eventAction, // e.g. 'addToCart'
          step: step, 
          pageType: ecommerce.pageType,
          purchase: !!ecommerce.purchase,
          data: data
        });
      }
    }); 
  }

  proxyGtmDataLayer();

  window.onerror = function (msg, url, lineNo, columnNo, error) {
    x.event({
      type: "error",
      hint: "window.onerror",
      data: {
        msg: msg,
        url: url,
        lineNo: lineNo,
        columnNo: columnNo,
        errorName: error ? error.name : null,
        errorMsg: error ? error.message : null,
        errorMsg: error ? error.stack : null,
        errorStack: error.stack
      }
    });
  };


  window.addEventListener("hashchange", function (e) {
    x.event({
      type: "hashchange",
      interaction: false,
      url: document.location      
    });
  }, false);

  window.addEventListener("popstate", function(e) {
    x.event({
      type: "popstate",
      interaction: false
    });
  }, false);

  window.addEventListener("load", function (e) {
    x.event({
      type: "load",
      interaction: false
    });
  }, false);

  window.addEventListener("unload", function(e) {
      x.event({
          type: "unload",
          interaction: false
      });
  }, false);


  document.addEventListener("DOMContentLoaded", function(event) {
    x.event({
      type: "DOMContentLoaded",
      interaction: false
    });
  }, false);

})();

